import time
import praw
import sqlite3
import OAuth2Util

# User config
# --------------------------------------------------------------------
# This is the source subreddit for the bot list. Don't include the /r/
SUBREDDIT_NAME = ''

# this is the page of the subs wiki the list is on
WIKI_PAGE = ''

# --------------------------------------------------------------------


def accept_mod_invites(r):
    print('Getting any mod invites...')

    for msg in r.get_unread():
        if msg.body.startswith('**gadzooks!'):
            sub = r.get_info(thing_id=msg.subreddit.fullname)
            print('Accepting {} invite'.format(str(sub)))
            try:
                sub.accept_moderator_invite()
            except praw.errors.InvalidInvite:
                continue
            msg.mark_as_read()



def get_bot_list(r):
    print('Getting bot list...')

    # get the wiki
    wiki = r.get_wiki_page(SUBREDDIT_NAME, WIKI_PAGE)

    # get the markdown and split it on newlines
    return [uname[3:] for uname in wiki.content_md.split('\r\n\r\n')]



def update_bots(sql, c, bots):
    print('Updating bot list...')

    bots_to_ban = []
    # check the list of bots for new ones
    for bot in bots:
        c.execute('SELECT uname from bots WHERE uname = ?', [bot])
        if not c.fetchone():
            bots_to_ban.append(bot)
            c.execute('INSERT INTO bots VALUES(?)', [bot])

    sql.commit()
    return bots_to_ban


def ban_bots(r, bots):
    print('Banning bots...')

    for sub in r.get_my_moderation():
        for bot in bots:
            try:
                sub.add_ban(bot)
            except praw.errors.InvalidUser:
                continue


def main():
    r = praw.Reddit(user_agent='subreddit_account_ban v1.0 /u/cutety')
    o = OAuth2Util.OAuth2Util(r, print_log=True)

    sql = sqlite3.connect('bots_ban_list.db')
    c = sql.cursor()

    c.execute('CREATE TABLE IF NOT EXISTS bots(uname TEXT)')
    sql.commit()

    accept_mod_invites(r)
    bot_lst = get_bot_list(r)
    bots = update_bots(sql, c, bot_lst)
    ban_bots(r, bots)


if __name__ == '__main__':
    main()